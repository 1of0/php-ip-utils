<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

use InvalidArgumentException;

/**
 * Mutable collection.
 *
 * This collection handles implementations of {@link NetworkLocationInterface} such as {@see IpAddress} and
 * {@see Subnet}.
 */
class Collection extends AbstractCollection
{
    /**
     * Adds the provided location to the collection.
     *
     * The location must be an object that implements the {@link NetworkLocationInterface}, or a string that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface|string $location
     * @return static
     *
     * @throws InvalidArgumentException if the provided location cannot be parsed
     */
    public function addItem($location): self
    {
        $this->addLocation($this->resolveLocation($location));
        return $this;
    }

    /**
     * Adds the provided locations to the collection.
     *
     * The locations must be objects that implement the {@link NetworkLocationInterface}, or strings that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface[]|string[] $locations
     * @return static
     *
     * @throws InvalidArgumentException if the provided locations contain items that cannot be parsed
     */
    public function addItems(array $locations): self
    {
        $this->addLocations($this->resolveLocations($locations));
        return $this;
    }

    /**
     * Filter out all redundant items in the collection.
     *
     * For example if the collection contains 192.168.0.0/24 and 192.168.0.1, the filtered collection will only
     * contain a {@link Subnet} instance for 192.168.0.0/24.
     *
     * @return static
     */
    public function filterOutRedundantItems(): self
    {
        $this->filterRedundant();
        return $this;
    }

    /**
     * Removes the provided location from the collection.
     *
     * The location must be an object that implements the {@link NetworkLocationInterface}, or a string that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface|string $location
     * @return static
     *
     * @throws InvalidArgumentException if the provided location cannot be parsed
     */
    public function removeItem($location): self
    {
        $this->removeLocation($this->resolveLocation($location));
        return $this;
    }

    /**
     * Removes the provided locations from the collection.
     *
     * The locations must be objects that implements the {@link NetworkLocationInterface}, or strings that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface[]|string[] $locations
     * @return static
     *
     * @throws InvalidArgumentException if the provided locations contain items that cannot be parsed
     */
    public function removeItems(array $locations): self
    {
        $this->removeLocations($this->resolveLocations($locations));
        return $this;
    }
}
