<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

/**
 * Provides a factory.
 *
 * The factory creates {@link IpAddress} and {@link Subnet} instances depending on the input.
 */
class Factory
{
    private static ?self $instance = null;

    /**
     * Returns the default instance of the factory.
     *
     * @return self
     */
    public static function get(): self
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Parses the string into an object.
     *
     * Determines whether the provided string is an {@link IpAddress} or {@link Subnet} and returns an instance of the
     * appropriate type. If it's neither of those objects this method will return null.
     *
     * @param string $ipOrRange
     * @return NetworkLocationInterface|null
     */
    public function parse(string $ipOrRange): ?NetworkLocationInterface
    {
        if (IpAddress::isValidIpAddress($ipOrRange)) {
            return IpAddress::parse($ipOrRange);
        }

        if (Subnet::isValidCidr($ipOrRange)) {
            return Subnet::parseCidr($ipOrRange);
        }

        return null;
    }
}
