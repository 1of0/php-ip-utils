<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

/**
 * Interface for classes representing a network location.
 *
 * Common ancestor to {@link IpAddress} and {@link Subnet}
 */
interface NetworkLocationInterface
{
    /**
     * Should return a string that uniquely identifies the represented location.
     *
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * Should return the canonical form of the represented location.
     *
     * @return string
     */
    public function toString(): string;

    /**
     * Should return a CIDR formatted string of the represented location.
     *
     * If an implementation does not support CIDR representation it must return null.
     *
     * @return string|null
     */
    public function getCidr(): ?string;

    /**
     * Should return the canonical form of the represented location.
     *
     * @return string
     */
    public function __toString();

    /**
     * Should return whether the instance is equal to the provided location.
     *
     * @param NetworkLocationInterface $location
     * @return bool
     */
    public function equals(NetworkLocationInterface $location): bool;

    /**
     * Should return whether the provided location is a subset of the location represented by the instance or
     * whether it is equal to it.
     *
     * @param NetworkLocationInterface $location
     * @return bool
     */
    public function contains(NetworkLocationInterface $location): bool;
}
