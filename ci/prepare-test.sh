#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install git libzip-dev zip -yqq

pecl install xdebug-2.8.1
docker-php-ext-install zip
docker-php-ext-enable xdebug

curl --location --output /usr/local/bin/composer https://getcomposer.org/composer-stable.phar
curl --location --output /usr/local/bin/phpcs https://github.com/squizlabs/PHP_CodeSniffer/releases/latest/download/phpcs.phar
curl --location --output /usr/local/bin/phpstan https://github.com/phpstan/phpstan/releases/latest/download/phpstan.phar

chmod +x /usr/local/bin/{composer,phpcs,phpstan}
