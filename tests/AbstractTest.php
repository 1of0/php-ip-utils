<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IpUtils\Test;

use Generator;
use OneOfZero\IpUtils\IpAddress;
use PHPUnit\Framework\TestCase;
use stdClass;

abstract class AbstractTest extends TestCase
{
    public const VALID_IP_ADDRESSES = [
        IpAddress::FAMILY_IPV4 => [
            '127.0.0.1',
            '255.255.255.255',
            '0.0.0.0',
            '127.1',
            '192.168.1',
            '127.000.000.001',
        ],
        IpAddress::FAMILY_IPV6 => [
            '::1',
            'fe80::',
            'FFFF::',
            'ffff::',
            '0000:0000:0000:0000:0000:0000:0000:0000',
            'ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff',
        ],
    ];

    public const VALID_CIDRS = [
        ['192.168.1.0/24', '192.168.1.0', 24],
        ['fe80::/48', 'fe80::', 48],
    ];

    public function getValidIpProvider(): array
    {
        return array_map('self::wrapInArray', array_merge(
            self::VALID_IP_ADDRESSES[IpAddress::FAMILY_IPV4],
            self::VALID_IP_ADDRESSES[IpAddress::FAMILY_IPV6]
        ));
    }

    public function getValidSimpleIpProvider(): array
    {
        return [
            ['127.0.0.1'],
            ['255.255.255.255'],
            ['0.0.0.0'],
            ['::1'],
            ['fe80::'],
            ['ffff::'],
            ['ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff'],
        ];
    }

    public function getInvalidIpProvider(): array
    {
        return [
            [''],
            ['foo'],
            [PHP_INT_MIN],
            [PHP_INT_MAX],
            [true],
            ['256.255.255.255'],
            ['2555.255.255.255'],
            ['l33t::'],
            ['fffff::'],
            ['192.168.1.0/24'],
            ['fe80::/48'],
        ];
    }

    public function getIpFamilyProvider(): Generator
    {
        foreach (self::VALID_IP_ADDRESSES as $family => $ips) {
            foreach ($ips as $ip) {
                yield [$ip, $family];
            }
        }
    }

    public function getCanonicalizationDataProvider(): array
    {
        return [
            ['127.1', '127.0.0.1'],
            ['192.168.1', '192.168.0.1'],
            ['127.000.000.001', '127.0.0.1'],
            ['0000:0000:0000:0000:0000:0000:0000:0000', '::'],
            ['0000:0000:0000:0000:0000:0000:0000:0001', '::1'],
            ['fe80:0000:0000:0000:0000:0000:0000:0001', 'fe80::1'],
        ];
    }

    public function getValidPrefixLengthProvider(): array
    {
        return [
            ['127.0.0.1', 16],
            ['127.0.0.1', 32],
            ['127.0.0.1', 0],
            ['::1', 128],
            ['::1', 64],
            ['::1', 0],
        ];
    }

    public function getInvalidPrefixLengthProvider(): array
    {
        return [
            ['127.0.0.1', 33],
            ['127.0.0.1', -1],
            ['127.0.0.1', PHP_INT_MAX],
            ['127.0.0.1', PHP_INT_MIN],
            ['::1', 129],
            ['::1', -1],
            ['::1', PHP_INT_MAX],
            ['::1', PHP_INT_MIN],
        ];
    }

    public function getValidContainsProvider(): array
    {
        return [
            ['192.168.0.0/16', '192.168.0.0'],
            ['192.168.0.0/16', '192.168.0.1'],
            ['192.168.0.0/16', '192.168.128.255'],
            ['192.168.0.0/16', '192.168.255.255'],
            ['a:b:c::/48', 'a:b:c::'],
            ['a:b:c::/48', 'a:b:c::1'],
            ['a:b:c::/48', 'a:b:c:d:ffff:ffff:ffff:ffff'],
            ['a:b:c::/48', 'a:b:c:ffff:ffff:ffff:ffff:ffff'],
        ];
    }

    public function getInvalidContainsProvider(): array
    {
        return [
            ['192.168.0.0/16', '192.167.255.255'],
            ['192.168.0.0/16', '192.169.9.0'],
            ['192.168.0.0/16', '0.0.0.0'],
            ['192.168.0.0/16', '255.255.255.255'],
            ['a:b:c::/48', 'a:b:b:ffff:ffff:ffff:ffff:ffff'],
            ['a:b:c::/48', 'a:b:d::'],
            ['a:b:c::/48', '::'],
            ['a:b:c::/48', 'ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff'],
        ];
    }

    public function getValidContainsSubnetProvider(): array
    {
        return [
            ['192.168.0.0/16', '192.168.0.0/24'],
            ['192.168.0.0/16', '192.168.1.0/24'],
            ['192.168.0.0/16', '192.168.255.0/24'],
            ['192.168.0.0/16', '192.168.255.0/24'],
            ['192.168.0.0/16', '192.168.0.0/17'],
            ['192.168.0.0/16', '192.168.0.0/32'],
            ['192.168.0.0/16', '192.168.255.255/32'],
            ['a:b:c::/48', 'a:b:c::/64'],
            ['a:b:c::/48', 'a:b:c::1/64'],
            ['a:b:c::/48', 'a:b:c:ffff::/64'],
            ['a:b:c::/48', 'a:b:c::/49'],
            ['a:b:c::/48', 'a:b:c:ffff:ffff:ffff:ffff:ffff/128'],
        ];
    }

    public function getInvalidContainsSubnetProvider(): array
    {
        return [
            ['192.168.0.0/16', '192.167.0.0/24'],
            ['192.168.0.0/16', '192.167.255.255/32'],
            ['192.168.0.0/16', '192.169.0.0/24'],
            ['192.168.0.0/16', '192.169.0.0/32'],
            ['192.168.0.0/16', '192.168.0.0/15'],
            ['a:b:c::/48', 'a:b:b::/64'],
            ['a:b:c::/48', 'a:b:b:ffff:ffff:ffff:ffff:ffff/128'],
            ['a:b:c::/48', 'a:b:d::/64'],
            ['a:b:c::/48', 'a:b:d::/128'],
            ['a:b:c::/48', 'a:b:c::/47'],
        ];
    }

    public function getValidCidrProvider(): array
    {
        return self::VALID_CIDRS;
    }

    public function getInvalidCidrProvider(): array
    {
        return [
            [''],
            ['foo'],
            [true],
            ['192.168.1.0'],
            ['192.168.1.0/'],
            ['192.168.1.0/-1'],
            ['192.168.1.0/33'],
            ['192.168.1.0/' . PHP_INT_MAX],
            ['192.168.1.0/' . PHP_INT_MIN],
            ['256.168.1.0/24'],
            ['ffff::/-1'],
            ['ffff::/129'],
            ['ffff::/' . PHP_INT_MAX],
            ['ffff::/' . PHP_INT_MIN],
            ['l33t::/48'],
            ['fffff::/48'],
        ];
    }

    public function getInvalidStringArguments(): array
    {
        return array_map('self::wrapInArray', [null, 0, true, new stdClass(), []]);
    }

    protected static function wrapInArray($item): array
    {
        return [$item];
    }
}
