<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IpUtils\Test;

use InvalidArgumentException;
use OneOfZero\IpUtils\Collection;
use OneOfZero\IpUtils\ImmutableCollection;
use OneOfZero\IpUtils\IpAddress;
use OneOfZero\IpUtils\Subnet;
use stdClass;

class CollectionTest extends AbstractTest
{
    /**
     * @dataProvider getValidItemsProvider
     * @param array $items
     */
    public function testConstructorValid(array $items): void
    {
        $this->assertEquals($items, (new Collection($items))->getItems());
        $this->assertEquals($items, (new ImmutableCollection($items))->getItems());
    }

    /**
     * @dataProvider getInvalidItemsProvider
     * @param array $items
     */
    public function testConstructorInvalid(array $items): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Collection($items);
    }

    /**
     * @dataProvider getInvalidItemsProvider
     * @param array $items
     */
    public function testImmutableConstructorInvalid(array $items): void
    {
        $this->expectException(InvalidArgumentException::class);
        new ImmutableCollection($items);
    }

    /**
     * @dataProvider getValidItemsProvider
     * @param array $items
     */
    public function testWithItemsValid(array $items): void
    {
        $this->assertEquals($items, (new Collection)->addItems($items)->getItems());
        $this->assertEquals($items, (new ImmutableCollection)->withItems($items)->getItems());

        $duplicatedItems = array_merge($items, $items);
        $this->assertEquals($items, (new Collection)->addItems($duplicatedItems)->getItems());
        $this->assertEquals($items, (new ImmutableCollection)->withItems($duplicatedItems)->getItems());
    }

    /**
     * @dataProvider getInvalidItemsProvider
     * @param array $items
     */
    public function testAddItemsInvalid(array $items): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new Collection)->addItems($items);
    }

    /**
     * @dataProvider getInvalidItemsProvider
     * @param array $items
     */
    public function testWithItemsInvalid(array $items): void
    {
        $this->expectException(InvalidArgumentException::class);
        (new ImmutableCollection)->withItems($items);
    }

    /**
     * @dataProvider getValidItemsProvider
     * @param array $items
     */
    public function testWithItemValid(array $items): void
    {
        $collection = new Collection();
        foreach ($items as $item) {
            $collection->addItem($item);
        }
        $this->assertEquals($items, $collection->getItems());

        $collection = new ImmutableCollection();
        foreach ($items as $item) {
            $collection = $collection->withItem($item);
        }
        $this->assertEquals($items, $collection->getItems());
    }

    /**
     * @dataProvider getInvalidItemsProvider
     * @param array $items
     */
    public function testAddItemInvalid(array $items): void
    {
        $this->expectException(InvalidArgumentException::class);
        $collection = new Collection();
        foreach ($items as $item) {
            $collection->addItem($item);
        }
    }

    /**
     * @dataProvider getInvalidItemsProvider
     * @param array $items
     */
    public function testWithItemInvalid(array $items): void
    {
        $this->expectException(InvalidArgumentException::class);
        $collection = new ImmutableCollection();
        foreach ($items as $item) {
            $collection = $collection->withItem($item);
        }
    }

    /**
     * @dataProvider getFilteredItemsProvider
     * @param array $redundantItems
     * @param array $expectedFilteredItems
     */
    public function testWithoutRedundantItems(array $redundantItems, array $expectedFilteredItems): void
    {
        $filteredCollection = (new Collection($redundantItems))->filterOutRedundantItems();
        $this->assertEquals($expectedFilteredItems, $filteredCollection->getItems());

        $filteredCollection = (new ImmutableCollection($redundantItems))->withRedundantItemsFilteredOut();
        $this->assertEquals($expectedFilteredItems, $filteredCollection->getItems());
    }

    public function testRemoveItem(): void
    {
        $collection = (new Collection)
            ->addItems(['192.168.0.1', '192.168.0.2', '192.168.0.3', '192.168.0.0/24'])
            ->removeItem('192.168.0.2');

        $this->assertCount(3, $collection->getItems());
        $this->assertFalse($collection->contains('192.168.0.2'));

        $collection = (new ImmutableCollection)
            ->withItems(['192.168.0.1', '192.168.0.2', '192.168.0.3', '192.168.0.0/24'])
            ->withoutItem('192.168.0.2');

        $this->assertCount(3, $collection->getItems());
        $this->assertFalse($collection->contains('192.168.0.2'));
    }

    public function testRemoveItems(): void
    {
        $collection = (new Collection)
            ->addItems(['192.168.0.1', '192.168.0.2', '192.168.0.3', '192.168.0.0/24'])
            ->removeItems(['192.168.0.2', '192.168.0.3']);

        $this->assertCount(2, $collection->getItems());
        $this->assertFalse($collection->contains('192.168.0.2'));
        $this->assertFalse($collection->contains('192.168.0.3'));

        $collection = (new ImmutableCollection)
            ->withItems(['192.168.0.1', '192.168.0.2', '192.168.0.3', '192.168.0.0/24'])
            ->withoutItems(['192.168.0.2', '192.168.0.3']);

        $this->assertCount(2, $collection->getItems());
        $this->assertFalse($collection->contains('192.168.0.2'));
        $this->assertFalse($collection->contains('192.168.0.3'));
    }

    /**
     * @dataProvider getValidItemsProvider
     * @param array $items
     */
    public function testStringRepresentations(array $items): void
    {
        $stringItems = array_map('strval', $items);

        $collection = (new Collection)->addItems($items);
        $this->assertEquals($stringItems, $collection->getStringRepresentations());

        $collection = (new ImmutableCollection)->withItems($items);
        $this->assertEquals($stringItems, $collection->getStringRepresentations());
    }

    /**
     * @dataProvider getValidItemsProvider
     * @param array $items
     */
    public function testIterator(array $items): void
    {
        $collection = (new Collection)->addItems($items);
        $reconstructedItems = [];
        foreach ($collection as $collectionItem) {
            $reconstructedItems[] = $collectionItem;
        }
        $this->assertEquals($items, $reconstructedItems);

        $collection = (new ImmutableCollection)->withItems($items);
        $reconstructedItems = [];
        foreach ($collection as $collectionItem) {
            $reconstructedItems[] = $collectionItem;
        }
        $this->assertEquals($items, $reconstructedItems);
    }

    /**
     * @dataProvider getValidItemsProvider
     * @param array $items
     */
    public function testCount(array $items): void
    {
        $this->assertCount(count($items), (new Collection)->addItems($items)->getItems());
        $this->assertCount(count($items), (new Collection)->addItems($items));
        $this->assertCount(count($items), (new ImmutableCollection)->withItems($items)->getItems());
        $this->assertCount(count($items), (new ImmutableCollection)->withItems($items));
    }

    public function testMutability(): void
    {
        $referenceA = new Collection();
        $this->assertCount(0, $referenceA);
        $referenceB = $referenceA->addItems(['127.0.0.1', '127.0.0.2', '127.0.0.2/32']);
        $this->assertCount(3, $referenceA);
        $this->assertSame($referenceA, $referenceB);
        $referenceC = $referenceB->removeItem('127.0.0.1');
        $this->assertCount(2, $referenceA);
        $this->assertSame($referenceB, $referenceC);
        $referenceD = $referenceC->filterOutRedundantItems();
        $this->assertCount(1, $referenceA);
        $this->assertSame($referenceC, $referenceD);
    }

    public function testImmutability(): void
    {
        $referenceA = new ImmutableCollection();
        $this->assertCount(0, $referenceA);
        $referenceB = $referenceA->withItems(['127.0.0.1', '127.0.0.2', '127.0.0.2/32']);
        $this->assertCount(0, $referenceA);
        $this->assertNotSame($referenceA, $referenceB);
        $referenceC = $referenceB->withoutItem('127.0.0.1');
        $this->assertCount(0, $referenceA);
        $this->assertNotSame($referenceB, $referenceC);
        $referenceD = $referenceC->withRedundantItemsFilteredOut();
        $this->assertCount(0, $referenceA);
        $this->assertNotSame($referenceC, $referenceD);
    }

    public function testContains(): void
    {
        $collection = (new ImmutableCollection)->withItems(['172.16.0.0/16', '10.0.0.0/8', '127.0.0.10']);
        $this->assertFalse($collection->contains('172.16.0.1'));
        $this->assertFalse($collection->contains('10.0.0.1'));
        $this->assertTrue($collection->contains('172.16.0.0/16'));
        $this->assertTrue($collection->contains('10.0.0.0/8'));
        $this->assertTrue($collection->contains('127.0.0.10'));
        $this->assertFalse($collection->contains('127.0.0.1'));
        $this->assertFalse($collection->contains('172.17.0.1'));
    }

    public function testContainsNested(): void
    {
        $collection = (new ImmutableCollection)->withItems(['172.16.0.0/16', '10.0.0.0/8', '127.0.0.10']);
        $this->assertTrue($collection->containsNested('172.16.0.1'));
        $this->assertTrue($collection->containsNested('10.0.0.1'));
        $this->assertTrue($collection->containsNested('127.0.0.10'));
        $this->assertFalse($collection->containsNested('127.0.0.1'));
        $this->assertFalse($collection->containsNested('172.17.0.1'));
        $this->assertTrue($collection->containsNested('10.100.0.0/16'));
        $this->assertFalse($collection->containsNested('192.168.0.0/16'));
    }

    public function getValidItemsProvider(): array
    {
        return [
            [[]],
            [[IpAddress::parse('192.168.0.1'), Subnet::parseCidr('172.16.0.0/16'), '127.0.0.1', '10.0.0.0/8']],
            [[IpAddress::parse('192.168.0.1'), IpAddress::parse('192.168.0.2')]],
            [[IpAddress::parse('192.168.0.1'), Subnet::parseCidr('192.168.0.0/24')]],
            [[Subnet::parseCidr('192.168.0.0/24'), IpAddress::parse('192.168.0.1')]],
            [[Subnet::parseCidr('192.168.0.0/24'), Subnet::parseCidr('172.16.0.0/16')]],
            [[Subnet::parseCidr('192.168.0.0/16'), Subnet::parseCidr('192.168.0.0/24')]],
            [[Subnet::parseCidr('192.168.0.0/24'), Subnet::parseCidr('192.168.0.0/16')]],
        ];
    }

    public function getFilteredItemsProvider(): array
    {
        return [
            [
                ['192.168.0.1', '192.168.0.1', '192.168.0.1'],
                [IpAddress::parse('192.168.0.1')]
            ],
            [
                ['192.168.0.0/24', '192.168.0.0/24', '192.168.0.0/24'],
                [Subnet::parseCidr('192.168.0.0/24')]
            ],
            [
                ['192.168.0.0/24', '192.168.0.1'],
                [Subnet::parseCidr('192.168.0.0/24')]
            ],
            [
                ['192.168.0.0/16', '192.168.0.0/24'],
                [Subnet::parseCidr('192.168.0.0/16')]
            ],
            [
                ['192.168.0.0/16', '192.168.0.0/24', '192.168.0.1'],
                [Subnet::parseCidr('192.168.0.0/16')]
            ],
            [
                ['192.168.0.1', '192.168.0.0/24', '192.168.0.0/16'],
                [Subnet::parseCidr('192.168.0.0/16')]
            ],
            [
                ['192.168.0.1/32', '192.168.0.1'],
                [Subnet::parseCidr('192.168.0.1/32')]
            ],
            [
                ['192.168.0.1', '192.168.0.1/32'],
                [Subnet::parseCidr('192.168.0.1/32')]
            ],
        ];
    }

    public function getInvalidItemsProvider(): array
    {
        return [
            [['127.0.0.1/33']],
            [['foo']],
            [[true]],
            [[null]],
            [[new stdClass()]],
            [[[]]],
        ];
    }
}
