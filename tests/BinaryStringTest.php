<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IpUtils\Test;

use Generator;
use InvalidArgumentException;
use OneOfZero\IpUtils\BinaryString;
use OverflowException;
use UnderflowException;

class BinaryStringTest extends AbstractTest
{
    /**
     * @dataProvider getHexToStringProvider
     * @param string $hex
     * @param string $string
     */
    public function testFromHexValid(string $hex, string $string): void
    {
        $this->assertEquals($string, BinaryString::fromHex($hex)->toString());
    }

    public function testFromHexInvalid(): void
    {
        $this->expectException(InvalidArgumentException::class);
        BinaryString::fromHex('foo');
    }

    /**
     * @dataProvider getValidDecimalProvider
     * @param int $decimal
     */
    public function testDecimalConversionValid(int $decimal): void
    {
        $this->assertEquals($decimal, BinaryString::fromUnsignedInteger($decimal)->toUnsignedInteger());
    }

    /**
     * @dataProvider getInvalidDecimalProvider
     * @param int $decimal
     */
    public function testDecimalConversionInvalid(int $decimal): void
    {
        $this->expectException(InvalidArgumentException::class);
        BinaryString::fromUnsignedInteger($decimal);
    }

    /**
     * @dataProvider getHexToStringProvider
     * @param string $hex
     */
    public function testToHex(string $hex): void
    {
        $this->assertEquals($hex, BinaryString::fromHex($hex)->toHex());
    }

    /**
     * @dataProvider getValidMaskProvider
     * @param int $prefixLength
     * @param int $maskSize
     * @param string $hexMask
     */
    public function testCreateMaskForPrefixLengthValid(int $prefixLength, int $maskSize, string $hexMask): void
    {
        $this->assertEquals($hexMask, BinaryString::createMaskForPrefixLength($prefixLength, $maskSize)->toHex());
    }

    /**
     * @dataProvider getInvalidMaskProvider
     * @param int $prefixLength
     * @param int $maskSize
     */
    public function testCreateMaskForPrefixLengthInvalid(int $prefixLength, int $maskSize): void
    {
        $this->expectException(InvalidArgumentException::class);
        BinaryString::createMaskForPrefixLength($prefixLength, $maskSize);
    }

    public function testIncrement(): void
    {
        for ($increment = 1; $increment <= 512; $increment+= 64) {
            $string = BinaryString::fromHex('000000');
            for ($i = 1; $i <= 256; $i++) {
                $string = $string->increment($increment);
                $this->assertEquals($i * $increment, $string->toUnsignedInteger());
            }
        }
    }

    /**
     * @dataProvider getInvalidDecimalProvider
     * @param int $increment
     */
    public function testIncrementInvalid($increment): void
    {
        $this->expectException(InvalidArgumentException::class);
        $string = BinaryString::fromHex('00');
        $string->increment($increment);
    }

    public function testIncrementOverflow(): void
    {
        $this->expectException(OverflowException::class);
        $string = BinaryString::fromHex('ff');
        $string->increment();
    }

    public function testDecrement(): void
    {
        $startHex = 'ffffff';
        $startDec = hexdec($startHex);

        for ($increment = 1; $increment <= 512; $increment+= 64) {
            $string = BinaryString::fromHex($startHex);
            for ($i = 1; $i <= 256; $i++) {
                $string = $string->decrement($increment);
                $this->assertEquals($startDec - $i * $increment, $string->toUnsignedInteger());
            }
        }
    }

    /**
     * @dataProvider getInvalidDecimalProvider
     * @param int $increment
     */
    public function testDecrementInvalid($increment): void
    {
        $this->expectException(InvalidArgumentException::class);
        $string = BinaryString::fromHex('00');
        $string->decrement($increment);
    }

    public function testDecrementOverflow(): void
    {
        $this->expectException(UnderflowException::class);
        $string = BinaryString::fromHex('00');
        $string->decrement();
    }

    /**
     * @dataProvider getBitManipulationProvider
     * @param string $a
     * @param string $b
     * @param string $and
     * @param string $or
     * @param string $xor
     * @param string $not
     */
    public function testBitManipulation(string $a, string $b, string $and, string $or, string $xor, string $not): void
    {
        $this->assertEquals($and, BinaryString::fromHex($a)->bitwiseAnd(BinaryString::fromHex($b))->toHex());
        $this->assertEquals($or, BinaryString::fromHex($a)->bitwiseOr(BinaryString::fromHex($b))->toHex());
        $this->assertEquals($xor, BinaryString::fromHex($a)->bitwiseXor(BinaryString::fromHex($b))->toHex());
        $this->assertEquals($not, BinaryString::fromHex($a)->bitwiseNot()->toHex());
        $this->assertEquals($not, BinaryString::fromHex($a)->bitwiseInvert()->toHex());
    }

    /**
     * @dataProvider getInvalidStringArguments
     * @param mixed $argument
     */
    public function testBitManipulationInvalid($argument): void
    {
        $this->expectException(InvalidArgumentException::class);
        BinaryString::fromHex('000000')->bitwiseAnd($argument);
    }

    public function testEquality(): void
    {
        $a = BinaryString::fromHex('ffff0000');
        $b = BinaryString::fromHex('0000ffff');

        $this->assertTrue($a->greaterThan($b));
        $this->assertTrue($a->greaterThanOrEqualTo($b));
        $this->assertTrue($b->lessThan($a));
        $this->assertTrue($b->lessThanOrEqualTo($a));

        $this->assertFalse($b->greaterThan($a));
        $this->assertFalse($b->greaterThanOrEqualTo($a));
        $this->assertFalse($a->lessThan($b));
        $this->assertFalse($a->lessThanOrEqualTo($b));

        $this->assertTrue($a->greaterThanOrEqualTo($a));
        $this->assertTrue($a->lessThanOrEqualTo($a));
        $this->assertTrue($b->greaterThanOrEqualTo($b));
        $this->assertTrue($b->lessThanOrEqualTo($b));

        $this->assertFalse($a->greaterThan($a));
        $this->assertFalse($a->lessThan($a));
        $this->assertFalse($b->greaterThan($b));
        $this->assertFalse($b->lessThan($b));

        $this->assertGreaterThan(0, $a->compare($b));
        $this->assertLessThan(0, $b->compare($a));
        $this->assertEquals(0, $a->compare($a));
    }

    public function getValidMaskProvider(): array
    {
        return [
            [16, 32, str_repeat('f', 4) . str_repeat('0', 4)],
            [24, 32, str_repeat('f', 6) . str_repeat('0', 2)],
            [28, 32, str_repeat('f', 7) . str_repeat('0', 1)],
            [32, 32, str_repeat('f', 8)],
            [0, 32, str_repeat('0', 8)],
            [48, 128, str_repeat('f', 12) . str_repeat('0', 20)],
            [64, 128, str_repeat('f', 16) . str_repeat('0', 16)],
            [68, 128, str_repeat('f', 17) . str_repeat('0', 15)],
            [128, 128, str_repeat('f', 32)],
            [0, 128, str_repeat('0', 32)],
            [0, 0, ''],
        ];
    }

    public function getInvalidMaskProvider(): array
    {
        return [
            [-1, 32],
            [33, 32],
            [0, -8],
            [15, 15],
        ];
    }

    public function getValidDecimalProvider(): array
    {
        return array_map('self::wrapInArray', [0, 1, PHP_INT_MAX]);
    }

    public function getInvalidDecimalProvider(): array
    {
        return array_map('self::wrapInArray', [-1, PHP_INT_MIN]);
    }

    public function getHexToStringProvider(): Generator
    {
        foreach (['foo', 'bar', 'baz', '1234567890'] as $string) {
            yield [bin2hex($string), $string];
        }
    }

    public function getBitManipulationProvider(): array
    {
        return [
            ['0f0f0f', 'f0f0f0', '000000', 'ffffff', 'ffffff', 'f0f0f0'],
            ['f0f0f0', '0f0f0f', '000000', 'ffffff', 'ffffff', '0f0f0f'],
            ['000000', 'ffffff', '000000', 'ffffff', 'ffffff', 'ffffff'],
            ['ffffff', '000000', '000000', 'ffffff', 'ffffff', '000000'],
            ['ffffff', 'ffffff', 'ffffff', 'ffffff', '000000', '000000'],
            ['000000', '000000', '000000', '000000', '000000', 'ffffff'],
        ];
    }
}
