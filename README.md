# 1of0/ip-utils

[![pipeline status](https://gitlab.com/1of0/php/ip-utils/badges/master/pipeline.svg)](https://gitlab.com/1of0/php/ip-utils/-/commits/master)
[![coverage report](https://gitlab.com/1of0/php/ip-utils/badges/master/coverage.svg)](https://gitlab.com/1of0/php/ip-utils/-/commits/master)

## What

A couple of simple wrapper classes / utilities to make handling of IP
addresses and subnets easier.

## License

This project is licensed under the MIT license. See [LICENSE.md](LICENSE.md).

## Requirements

- `php >= 7.4`

## Installation

```
composer require 1of0/ip-utils
```

## Why

Disclaimer: there are some other libraries around that may be more extensive:

- [S1lentium/IPTools](https://github.com/S1lentium/IPTools)
- [darsyn/ip](https://github.com/darsyn/ip)
- [mlocati/ip-lib](https://github.com/mlocati/ip-lib)

However, one didn't exactly have the interface I was envisioning, one
depended on `bcmath`, and one had some approaches that I didn't really agree
with. That said, they all seem to have good test coverage and in-the-field
usage, so take your pick!

So mainly I built this to get the interface that I had in mind, and to have zero
dependencies other than `php >= 7.4`.

## Usage

See the generated [phpdoc](https://1of0.gitlab.io/php/ip-utils/namespaces/oneofzero-iputils.html)
for a full API specification.

### Basic usage

```php
/** @noinspection ForgottenDebugOutputInspection */
use OneOfZero\IpUtils\Factory;
use OneOfZero\IpUtils\IpAddress;
use OneOfZero\IpUtils\Subnet;

/** @var IpAddress $ip */
$ip = Factory::get()->parse('192.168.0.123');

/** @var Subnet $subnet */
$subnet = Factory::get()->parse('192.168.0.0/24');

var_dump($subnet->contains($ip));
// bool(true)

print_r([
    (string)$subnet->getAddress(),
    (string)$subnet->getNetworkAddress(),
    (string)$subnet->getRouterAddress(),
    (string)$subnet->getBroadcastAddress(),
    (string)$subnet->getSubnetMask(),
    $subnet->getCidr(),
    $subnet->getPrefixLength(),
    $subnet->getIdentifier(),
]);
// Array
// (
//     [0] => 192.168.0.0
//     [1] => 192.168.0.0
//     [2] => 192.168.0.1
//     [3] => 192.168.0.255
//     [4] => 255.255.255.0
//     [5] => 192.168.0.0/24
//     [6] => 24
//     [7] => c0a80000ffffff00
// )
```

### Collections

Also check out `ImmutableCollection`!

```php
/** @noinspection ForgottenDebugOutputInspection */
use OneOfZero\IpUtils\IpAddress;
use OneOfZero\IpUtils\Subnet;
use OneOfZero\IpUtils\Collection;

$collection = new Collection([
    '192.168.0.123',
    '10.0.0.0/8',
    IpAddress::parse('127.0.0.1'),
    Subnet::parseCidr('192.168.0.0/24'),
]);

$collection->addItem('::1');

print_r($collection->getStringRepresentations());
// Array
// (
//     [0] => 192.168.0.123
//     [1] => 10.0.0.0/8
//     [2] => 127.0.0.1
//     [3] => 192.168.0.0/24
//     [4] => ::1
// )

$collection->filterOutRedundantItems();

print_r($collection->getStringRepresentations());
// Array
// (
//     [0] => 10.0.0.0/8
//     [1] => 127.0.0.1
//     [2] => 192.168.0.0/24
//     [3] => ::1
// )
```

## Specific design choices

*It's not a bug, it's a feature!*

### Subnet behaviour

When a `Subnet` instance is created with an IP address that is not the
network address (e.g. `192.168.0.10/24` instead of `192.168.0.0/24`), the IP
address **will be** retained and returned via `getAddress()` and `getCidr()`.

However, it **will not** be used by methods such as `contains()`, `equals()`,
and `getIdentifier()`.

This affects the behaviour of the `Subnet` in a `Collection`.

For example:

```php
use OneOfZero\IpUtils\Collection;

$collection = new Collection(['192.168.0.10/24']);

$collection->getStringRepresentations();
// ['192.168.0.10/24']

$collection->addItem('192.168.0.20/24');

$collection->getStringRepresentations();
// ['192.168.0.20/24'] first item was replaced because both Subnets have the same identifier
```
